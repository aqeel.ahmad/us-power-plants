# Tech

## Design

This implementation is inspired by the hexagonal and the onion architecture.

At start up, the application loads, parses and inserts a CSV file into an embeded sqlite DB. This process is done as a DB migration so as long as the DB file is present to further reinsert of the data is present. The implementation details can be checked directly in the [migration file](../api/infra/migrations/01_persist_egrid2020.ts).

A simple authentication header is required to access all endpoints. It is called `token` and its value for be `Bearer xxx`.
For implementation details, check the [authenticator function](../api/infra/routes/auth.ts).

A high lever overview of the application could be like the following

![Architecture](archi.png)

----

## Technological choises

### Backend

#### implementation

* The api is provided by an `express` service using `typescript`
* The typesafe routing and API documentation is provided by `typera`
* Compile type safety is improved by both `typera` and `fp-ts`
* Test are implemented in `jest`
* The api follows insdustry standand conventions of RESTful
* Linting is done using `typescript-eslint`
* DB access is provided by `sequelize`
* DB migrations are handled by [`umzug`](https://github.com/sequelize/umzug)
* The embeded DB is `sqlite`

#### Discussion

As stated before, this implementation is inspired by the `hexagonal` architecture, as such, the `domain` is independent of the implementation.

A tree view of the folder structure might help you

```
├── domain
├── infra
│   ├── db
│   ├── migrations
│   ├── routes
│   ├── server
│   └── utilities
```

### Frontend

* `react` is used for the SPA
* `leaflet` provides the Map visualisation

A tree view of the folder structure might help you

```
└── tsx
    └── powerplant
        ├── clients
        │   └── power-plant-client.ts
        ├── components
        │   └── power-plant-map.tsx
        └── models
            └── power-plant.ts
```

----

## Future improvements

* Security token handling could be improved, an extensive discussion is done in [common.ts](../api/infra/routes/common.ts).
* Add some integration tests
