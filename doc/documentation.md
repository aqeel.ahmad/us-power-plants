# Documentation

### API documentation

```
$ make api-doc && make api-start
```

browse to http://localhost:9000/api-docs/

Remember to use a `token` of this kind:

```
$ Bearer something
```

### Code documentation

In general the code is self explanatory and types help reduce the unknowns. Some libraries are not realy explicit so some improvement could be done to increase the readability is some sections. 