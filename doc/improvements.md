# Improvements

* Increase both the quality and the amount of tests
* Find an streaming (based on promises) CSV parser that does not have memory leaks
