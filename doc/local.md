# Running in a local environment

## Prerequisites

To build/run the project you need `yarn > 1.22.X`  and `node > v16.14.x`.


## Building

First of all, you need to install all the dependencies:

```
$ make install
```

## Running

```
$ make api-start
```

Please wait until all data is loaded, then you can run:

```
$ make web-start
```

Then, you could browse the api `http://localhost:9000/api-docs`
or take a look at the application in here `http://localhost:3000/`

## Testing the API

You could either:

* `curl` directly the api:

```
$  curl -X 'GET' \
  'http://localhost:9000/api/power-plants/search' \
  -H 'accept: application/json' \
  -H 'token: Bearer toto' | jq
```

* Use the `OpenAPI` documentation client`http://localhost:9000/api-docs`. Do not forget the mandatory `token` header.

## Using docker

You have some tasks available in the make file for working with docker:

```
$ make api-docker-build
```

then:

```
$ make api-docker-run
```

If another container already exist you could remove it with:

```
$ make api-docker-rm
```

If you want to take a look at the logs, you can do:

```
$ make api-docker-logs
```
