export interface PowerPlant {
  id: string;
  name: string;
  state: string;
  annual_net_generation?: number;
  latitude: number;
  longitude: number;
}

export interface PowerPlantFetcher {
  fecthAll: (
    limit: number,
    selectedState: string | undefined
  ) => Promise<PowerPlant[]>;
  fetchStates: () => Promise<string[]>;
}
