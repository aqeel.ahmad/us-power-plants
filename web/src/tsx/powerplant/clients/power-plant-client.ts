import { PowerPlantFetcher } from "../models/power-plant";

const API_URL = "http://localhost:9000";

const requestOptions = {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
    token: "Bearer  5a314bc0-ab6a-11ec-83d1-6c40089b116e",
  },
};

const httpPPFetcher: PowerPlantFetcher = {
  fecthAll: async (limit, state) => {
    const stateFilter = state ? `/state/${state}` : "";
    return fetch(
      `${API_URL}/api/power-plants/search${stateFilter}?limit=${limit}`,
      requestOptions
    ).then((response) => response.json());
  },
  fetchStates: async () => {
    return fetch(`${API_URL}/api/states`, requestOptions).then((response) =>
      response.json()
    );
  },
};

export default httpPPFetcher;
