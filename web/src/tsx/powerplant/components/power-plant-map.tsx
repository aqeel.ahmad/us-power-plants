import React, { useState, useEffect, ChangeEvent } from "react";
import { Map, Marker, TileLayer, Popup, Tooltip } from "react-leaflet";

import powerPlantFetcher from "../clients/power-plant-client";
import { PowerPlant } from "../models/power-plant";

/* eslint-disable @typescript-eslint/no-unused-vars */

const PowerPlantMap = () => {
  const US_CENTER = [39.5, -98.35];
  const ZOOM = 3;
  const [states, setStates] = useState<string[]>([]);
  const [selectedState, setSelectedState] = useState<string | undefined>(
    undefined
  );
  const [limit, setLimit] = useState<number>(200);
  const [powerPlants, setPowerPlants] = useState<PowerPlant[]>([]);

  const handleLimitChange = (e: ChangeEvent<HTMLInputElement>) => {
    const newLimit: number = parseInt(e.target.value) || 0;
    setLimit(newLimit);
  };

  const handleStateChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;
    const updatedSelectedState = value === "All states" ? undefined : value;
    setSelectedState(updatedSelectedState);
  };

  useEffect(() => {
    powerPlantFetcher.fetchStates().then((data) => setStates(data));
  }, []);

  useEffect(() => {
    powerPlantFetcher
      .fecthAll(limit, selectedState)
      .then((data) => setPowerPlants(data));
  }, [limit, selectedState]);

  return (
    <div>
      <div style={{ display: "flex", justifyContent: "center", gap: "10px" }}>
        <div style={{ display: "flex", gap: "5px" }}>
          <label>Filter by state:</label>
          <select style={{ width: 150 }} onChange={handleStateChange}>
            <option>All states</option>
            {states.map((state, index) => (
              <option key={index} value={state}>
                {state}
              </option>
            ))}
          </select>
        </div>
        <input
          type="range"
          min={0}
          max={1000}
          value={limit}
          onChange={(e) => handleLimitChange(e)}
        />
      </div>
      <h3>{powerPlants.length} Power Plants were found! </h3>
      <Map center={US_CENTER} zoom={ZOOM}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        {powerPlants.map((powerPlant) => {
          return (
            <Marker
              opacity={10}
              key={powerPlant.id}
              position={[powerPlant.latitude, powerPlant.longitude]}
              onClick={() => console.log(powerPlant)}
            >
              <Popup>{powerPlant.name}</Popup>
              <Tooltip>{powerPlant.annual_net_generation} MWh</Tooltip>
            </Marker>
          );
        })}
      </Map>
    </div>
  );
};

export default PowerPlantMap;
