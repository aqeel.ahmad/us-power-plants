import React from "react"; // do not remove
import PowerPlantMap from "./tsx/powerplant/components/power-plant-map";

import "./App.css";

const App = () => {
  return (
    <div className="App">
      <h1>Energy generation in the US</h1>
      <PowerPlantMap />
    </div>
  )
}

export default App;
