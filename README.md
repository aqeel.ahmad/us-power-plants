AIQ PowerPlant technical challenge
=====

## Context

* [Technical decisions](doc/adr.md)
* [Configuring the dev environment](doc/local.md)
* [Documentation](doc/documentation.md)

TLDR;

* You'll need `nodeJs` and `yarn`
* This is an `express` rest service
* It uses an embeded `sqlite` database to store data from a CSV file
* The api documentation is compatible with `OpenApiv3` and can be consumed with a swagger client (accesible at `/api-docs`)
* you need a `token` header to query the endpoints (eg `token: Bearer challenge`)
