.PHONY: install
install:
	@cd api && rm db.sqlite 2>/dev/null || true
	@cd api && yarn install
	@cd web && yarn install

.PHONY: api-build
api-build: api-lint api-doc
	@cd api && yarn run build

.PHONY: api-start
api-start:
	@cd api && yarn run serve

.PHONY: api-clean
api-clean:
	@cd api && rm db.sqlite 2>/dev/null || true

.PHONY: api-test
api-test:
	@cd api && yarn run test:unit

.PHONY: api-lint
api-lint:
	@cd api && yarn run lint

api-doc:
	@cd api && npx typera-openapi infra/routes/index.ts

.PHONY: api-docker-build
api-docker-build:
	@docker build -t fagossa/api-challenge api/

.PHONY: api-docker-run
api-docker-run:
	@docker run -d --name pp-challenge -p 9000:9000 fagossa/api-challenge

.PHONY: api-docker-rm
api-docker-rm:
	@docker rm --force $$(docker ps -aqf "name=pp-challenge")

.PHONY: api-docker-logs
api-docker-logs:
	@docker logs -f $$(docker ps -aqf "name=pp-challenge")

.PHONY: api-docker-exec
api-docker-exec:
	@docker exec -it $$(docker ps -aqf "name=pp-challenge") /bin/bash

.PHONY: web-start
web-start:
	@cd web && yarn run start
