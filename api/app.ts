import server from './infra/server';
import logger from "./infra/utilities/logger";
import config from 'config';

const port = config.get('server.port');
const host = config.get('server.host');

server
    .listen(port, host, () => {
        logger.info(`server running on ${host}:${port}`);
        console.log(`UP and running on ${host}:${port}.`);
    })
    .on('error', (e) => logger.error(e));
