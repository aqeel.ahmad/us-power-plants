import * as reader from 'fp-ts/lib/Reader'
import * as option from "fp-ts/lib/Option";

export interface PowerPlant {
    /** This entity represents a Power Plant data */
    id: string
    name: string
    state: string
    annual_net_generation: number
    latitude: number
    longitude: number
}

export interface PowerPlantWithStatePercentage extends PowerPlant {
    percentage: number
}

export interface PowerPlantRepository {
    get:    (id: string)    => Promise<option.Option<PowerPlant>>,
    search: (limit: number) => Promise<PowerPlant[]>,
    searchTop: (n: number)  => Promise<PowerPlant[]>,
    searchByState: (state: string) => Promise<PowerPlant[]>,
    getUniqueStates: () => Promise<string[]>,
}

const MAX_POWER_PLANTS = 50000; // arbitrary hard limit
const MIN_POWER_PLANTS = 10;

type PowerPlantService<T> = reader.Reader<PowerPlantRepository, Promise<T>>

export const getPowerPlant = (id: string): PowerPlantService<option.Option<PowerPlant>> => 
    (repo) => repo.get(id);

export const searchPowerPlants = (limit?: number): PowerPlantService<PowerPlant[]> => 
    (repo) => {
        const safeLimit: number = Math.min(limit || MIN_POWER_PLANTS, MAX_POWER_PLANTS);
        return repo.search(safeLimit)
    };

export const searchTopPowerPlant = (limit?: number): PowerPlantService<PowerPlant[]> => 
    (repo) => {
        const safeLimit: number = Math.min(limit || MIN_POWER_PLANTS, MAX_POWER_PLANTS);
        return repo.searchTop(safeLimit)
    };

export const searchPowerPlantByState = (state: string): PowerPlantService<PowerPlant[]> => 
    (repo) => repo.searchByState(state);

export const searchStatePercentagePowerPlant = (state: string): PowerPlantService<PowerPlantWithStatePercentage[]> => 
    (repo) => {
        return repo.searchByState(state)
            .then((arr) => {
                const total = arr.reduce((acc, curr) => acc + curr.annual_net_generation, 0);
                return arr
                    .map((pp) => {
                        return {
                            ...pp, 
                            percentage: total > 0 ? pp.annual_net_generation/total : 0
                        }
                    })
            })
    };

export const getUniqueStates = (): PowerPlantService<string[]> => 
    (repo) => repo.getUniqueStates();
