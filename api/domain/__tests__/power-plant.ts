import * as option from "fp-ts/lib/Option";

import { searchStatePercentagePowerPlant, PowerPlantRepository, PowerPlant } from '../power-plant'

/* eslint-disable @typescript-eslint/no-unused-vars */

const all_data: Array<PowerPlant> = [
  {
      id: '816e6cfc-aaef-11ec-a2aa-6c40089b116e',
      name: "Power plant#1",
      state: "AK",
      annual_net_generation: 641125,
      latitude: 63.210689,
      longitude: -143.247156
  },{
      id: '87f4a172-aaef-11ec-9b01-6c40089b116e',
      name: "Power plant#2",
      state: "AK",
      annual_net_generation: 1000,
      latitude: 63.210689,
      longitude: -143.247156
  },
  {
      id: 'a9f9453e-aaef-11ec-b2fd-6c40089b116e',
      name: "Power plant#3",
      state: "FL",
      annual_net_generation: 641125,
      latitude: 63.210689,
      longitude: -143.247156
  },
]

const withPPMockRepo: PowerPlantRepository = {
    search: async (limit) => Promise.resolve(all_data),
    searchTop: async (limit) => Promise.resolve(all_data),
    searchByState: async (state) => Promise.resolve(all_data.filter((pp)=>pp.state==state)),
    get: async (id) => Promise.resolve(option.some(all_data[0])),
};

describe('power-plant', () => {
  it('should return the contents of the repository', async () => {
    const testCases = [{
      state: 'AK',
      size: 2,
      percentages: ["0.0016", "0.9984"]
    },{
      state: 'FL',
      size: 1,
      percentages: [ "1.0000" ]
    }]
    testCases.forEach(async (test) => {
      const response = await searchStatePercentagePowerPlant(test.state)(withPPMockRepo)
      expect(response).toHaveLength(test.size)
      expect(response.map((pp) => pp.percentage.toFixed(4)).sort()).toEqual(test.percentages)
    })
  })
})
