import fs from 'fs';
import * as option from "fp-ts/lib/Option";
import { parse } from 'csv-parse';
import { v4 as uuidv4 } from 'uuid';

import logger from "../../infra/utilities/logger";
import { Migration, CSVReader } from '../db/migration';
import { PowerPlantModel, mapperPPToModel } from '../db/power-plant-model';
import { PowerPlant } from '../../domain/power-plant'

/* eslint-disable @typescript-eslint/no-explicit-any */

interface RawPowerPlantData {
    PNAME: string // Plant name
    PSTATABB: string // Plant state abbreviation
    PLNGENAN: string // Plant annual net generation (MWh)
    LAT: string // Plant latitude
    LON: string // Plant longitude
}

const safeString = (str: string): string => 
	str ? str.replace(/\s/g, "") : ""

const parseLineIntoPowerPlant: (value: string[], line: number) => option.Option<PowerPlant> = 
    (rawJson, line) => {
        try {
			// Note: another limitation of the current csv parsing library is
			// the lack of named fields
			const parsedObj: RawPowerPlantData = {
				PNAME: rawJson[3],
				PSTATABB: rawJson[2],
				PLNGENAN: rawJson[39],
				LAT: rawJson[19],
				LON: rawJson[20],
			}
			const parsedPlant = <PowerPlant> {
				id: uuidv4(),
				name: safeString(parsedObj.PNAME),
				state: safeString(parsedObj.PSTATABB),
				annual_net_generation: Number.parseInt(parsedObj.PLNGENAN.replace(/\s/g, "")),
				latitude: Number.parseFloat((parsedObj.LAT || '0').replace(',', ".")),
				longitude: Number.parseFloat((parsedObj.LON || '0').replace(',', "."))
			}
			return option.some(parsedPlant)
        } catch (err) {
			logger.warn(`error parsing data in line ${line}`, err)
            return option.none // TODO: maybe use an Either instead?
        }
    }

// Note: this could have an impact in memory, it could be wise to
// finc a csv parser that supports stream and has correct thread
// utilisation
const readCsv: CSVReader<PowerPlant> = 
	(fileContent, f) => {
		return new Promise((resolve, reject) => {
			parse(fileContent, {
				fromLine: 2,
			}, (error, result: string[][]) => {
				if (error) {
					reject(error);
				}
				resolve(result.map((raw, i) => f(raw, i)));
			});
		})
	}

export const up: Migration = async ({ context: sequelize }) => {
	const transaction = await sequelize.transaction();
	try {
		const fileContent = fs.readFileSync(`./csv/egrid2020_PLNT20.csv`, { encoding: 'utf-8' });
		const unfilteredPP: option.Option<PowerPlant>[] = await readCsv(
			fileContent,
			parseLineIntoPowerPlant
		)
		// Note: 'collect' does not exist in javascript, we must do a filter + map
		const filteredPP: PowerPlant[] = unfilteredPP
			.filter((mpp) => option.isSome(mpp))
			.map((pp) => option.getOrElse(() => undefined)(pp))

		logger.info(`Data migration: persisting <${filteredPP.length}> power plants`)
		
		Promise.all(
			filteredPP.map(pp => PowerPlantModel.create(mapperPPToModel(pp) as any)) // downcasting was necesary for sequalize
		)
		await transaction.commit();
    } catch (err) {
		logger.error('Error migrating data', err);
		await transaction.rollback();
		throw err;
    }
}

export const down: Migration = async ({ context: sequelize }) => {
	await sequelize.getQueryInterface().bulkDelete('power_plants', {}, {});
};
