import { DataTypes } from 'sequelize';
import { Migration } from '../db/migration';

export const up: Migration = async ({ context: sequelize }) => {
	const transaction = await sequelize.transaction();
	try {
		const queryInterface = sequelize.getQueryInterface()
		await queryInterface.createTable('power_plants', {
			id: {
				type: DataTypes.STRING,
				allowNull: false,
				primaryKey: true
			},
			name: {
				type: DataTypes.STRING,
				allowNull: false
			},
			state: {
				type: DataTypes.STRING,
				allowNull: false
			},
			annual_net_generation: {
				type: DataTypes.NUMBER,
				allowNull: true
			},
			longitude: {
				type: DataTypes.NUMBER,
				allowNull: false
			},
			latitude: {
				type: DataTypes.NUMBER,
				allowNull: false
			},
			createdAt: {
				type: DataTypes.DATE,
				allowNull: false
			},
			updatedAt: {
				type: DataTypes.DATE,
				allowNull: false
			}
		}, { transaction });
		await queryInterface.addIndex('power_plants', ['state'], { transaction });
		await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
}

export const down: Migration = async ({ context: sequelize }) => {
	await sequelize.getQueryInterface().dropTable('power_plants');
};
