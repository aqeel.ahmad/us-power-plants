import { router } from 'typera-express'

import { 
    searchPowerPlantsRoute, 
    searchTopPowerPlantsRoute, 
    getPowerPlantDetailRoute,
    searchPowerPlantsByStateRoute,
    searchPercentagePowerPlantsRoute,
    getStatesRoute,
} from './powerplant-routes';

export default router(
    searchPowerPlantsRoute,
    searchTopPowerPlantsRoute,
    getPowerPlantDetailRoute,
    searchPowerPlantsByStateRoute,
    searchPercentagePowerPlantsRoute,
    getStatesRoute,
);
