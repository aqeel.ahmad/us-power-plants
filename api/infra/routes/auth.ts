import { Response, Middleware } from 'typera-express'

import { User } from '../../domain/user'
import logger from "../utilities/logger";

// TODO: implement a real token validation
const authUserUsing: (string) => Promise<User> = 
    (token) => {
        const dummyUser: User = {
            login: 'johndoe@gmail.com',
            token,
        }
        return Promise.resolve<User>(dummyUser)
    }

export const authenticateUser: Middleware.Middleware<
  { user: User },
  Response.Unauthorized<string>
> = async (baseReq) => {
    const authHeader = baseReq.req.headers["token"]
    const safeAuthToken: string = Array.isArray(authHeader) ? authHeader[0] : authHeader || ''
    if (safeAuthToken.startsWith("Bearer ")) {
        const token = safeAuthToken.substring(7, safeAuthToken.length);
        const user = await authUserUsing(token)
        if (user) {
            logger.info("User authentified")
            return Middleware.next({ user })    
        }
        return Middleware.stop(Response.unauthorized('Not logged in'))
    } else {
        return Middleware.stop(Response.unauthorized('Invalid bearer'))
    }
}
