import * as t from 'io-ts'
import { Response, Parser } from 'typera-express'

/*
 * OpenAPI do not support the Autorization header
 * https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.0.md#fixed-fields-10
 * If in is "header" and the name field is "Accept", "Content-Type" or "Authorization", 
 * the parameter definition SHALL be ignored.
 * 
 * But the header is indeed supported in the RFC6750
 * https://www.rfc-editor.org/rfc/rfc6750
 * 
 * By the time being, we use the 'token' header until security schemes
 * are supported by typera.
 * 
 * Relevant documentation on security schemes is here: https://swagger.io/docs/specification/authentication/
 */
export const authHeader = Parser
    .headers(t.strict({ token: t.string }))

// Alias to avoid huge return types for lists
export type ResponseListOf<T> = Response.Ok<T> | 
    Response.BadRequest<string> | 
    Response.Unauthorized<string> | 
    Response.InternalServerError<string>

// Alias to avoid huge return types for single responses
export type ResponseElementOf<T> = Response.Ok<T> | 
    Response.BadRequest<string> | 
    Response.Unauthorized<string> | 
    Response.NotFound<string> | 
    Response.InternalServerError<string>
