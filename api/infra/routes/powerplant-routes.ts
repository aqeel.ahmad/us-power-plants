import * as t from 'io-ts'
import * as option from "fp-ts/lib/Option";
import { IntFromString } from 'io-ts-types/lib/IntFromString'
import { Route, Response, Parser, route } from 'typera-express'

import { authHeader, ResponseElementOf, ResponseListOf } from './common'
import { authenticateUser } from './auth'
import {
  PowerPlant,
  searchPowerPlants,
  searchTopPowerPlant, 
  getPowerPlant,
  searchPowerPlantByState,
  searchStatePercentagePowerPlant,
  getUniqueStates,
 } from '../../domain/power-plant'
import withPPSqlRepoImpl from '../db/power-plant-repo'

/**
 * Searches all power plants.
 * 
 * If no limit is specified, the response by default limits to 10 elements
 * 
 * @summary Search all power plants
 * 
 * @response 200 Power plants searched succesfully
 * @response 400 Validation error
 * @response 403 Authentication error
 * @response 500 Internal error
 */
export const searchPowerPlantsRoute: Route<ResponseListOf<PowerPlant[]>> 
= route
  .get('/power-plants/search')
  .use(Parser.query(t.partial({ limit: IntFromString })))
  .use(authHeader)
  .use(authenticateUser)
  .handler(async (req) => {
    return searchPowerPlants(req.query.limit)(withPPSqlRepoImpl)
        .then(result => Response.ok(result))
        .catch(error => Response.internalServerError(error))
  })

/**
 * Searches the top N power plants by energy production independent of where they 
 * are located.
 * 
 * If no limit is specified, the response by default limits to 10 elements
 * 
 * @summary Search top N power plants
 * 
 * @response 200 Power plants searched succesfully
 * @response 400 Validation error
 * @response 403 Authentication error
 * @response 500 Internal error
 */
 export const searchTopPowerPlantsRoute: Route<ResponseListOf<PowerPlant[]>> 
 = route
   .get('/power-plants/search/top')
   .use(Parser.query(t.partial({ limit: IntFromString })))
   .use(authHeader)
   .use(authenticateUser)
   .handler(async (req) => {
     return searchTopPowerPlant(req.query.limit)(withPPSqlRepoImpl)
         .then(result => Response.ok(result))
         .catch(error => Response.internalServerError(error))
   })
 
/**
 * Searches the actual and percentage value for plants of a state.
 * 
 * @summary Search actual and percentage of plants' federal state
 * 
 * @response 200 Power plants searched succesfully
 * @response 400 Validation error
 * @response 403 Authentication error
 * @response 500 Internal error
 */
 export const searchPercentagePowerPlantsRoute: Route<ResponseListOf<PowerPlant[]>> 
 = route
   .get('/power-plants/search/:state/percentage')
   .use(authHeader)
   .use(authenticateUser)
   .handler(async (req) => {
     return searchStatePercentagePowerPlant(req.routeParams.state)(withPPSqlRepoImpl)
         .then(result => Response.ok(result))
         .catch(error => Response.internalServerError(error))
   })
 
/**
 * Searches all plants located in a particular state.
 * 
 * @summary Search power plants in a state
 * 
 * @response 200 Power plants searched succesfully
 * @response 400 Validation error
 * @response 403 Authentication error
 * @response 500 Internal error
 */
 export const searchPowerPlantsByStateRoute: Route<ResponseListOf<PowerPlant[]>> 
 = route
   .get('/power-plants/search/state/:state')
   .use(authHeader)
   .use(authenticateUser)
   .handler(async (req) => {
     return searchPowerPlantByState(req.routeParams.state)(withPPSqlRepoImpl)
         .then(result => Response.ok(result))
         .catch(error => Response.internalServerError(error))
   })
 
/**
 * Gets a Power plant using an id
 * 
 * @summary Gives the details of a power plant
 * 
 * @response 200 Power plant detail
 * @response 400 Validation error
 * @response 403 Authentication error
 * @response 404 Element with id not found
 * @response 500 Internal error
 */
export const getPowerPlantDetailRoute: Route<ResponseElementOf<PowerPlant>>
= route
  .get('/power-plants/:id')
  .use(authHeader)
  .use(authenticateUser)
  .handler(async (req) => {
    return getPowerPlant(req.routeParams.id)(withPPSqlRepoImpl)
        .then(result => {
          return option
            .fold<PowerPlant, Response.Ok<PowerPlant> | Response.NotFound<string>>(
              () => Response.notFound<string>(`${req.routeParams.id}`), 
              r  => Response.ok(r))(result)
        })
        .catch(error => Response.internalServerError(error))
  })


/**
 * Returns list of unique states.
 * 
 * @summary Provides array of unique states
 * 
 * @response 200 States return successfully
 * @response 403 Authentication error
 * @response 500 Internal error
 */
 export const getStatesRoute: Route<ResponseListOf<string[]>> 
 = route
   .get('/states')
   .use(authHeader)
   .use(authenticateUser)
   .handler(async (req) => {
     return getUniqueStates()(withPPSqlRepoImpl)
         .then(result => Response.ok(result))
         .catch(error => Response.internalServerError(error))
   })
