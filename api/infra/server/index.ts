import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';

// routes
import mainRouter from '../routes';
import mainRouterDefs from '../routes/index.openapi';
import * as dbmigration from '../db/migration';

// api-doc
import { OpenAPIV3 } from 'openapi-types'
import * as swaggerUi from 'swagger-ui-express'
import { prefix } from 'typera-openapi'

dbmigration.migrate()

const openapiDoc: OpenAPIV3.Document = {
    openapi: '3.0.0',
    info: {
      title: 'Energy lookup',
      version: '0.1.0',
    },
    paths: {
      ...prefix('/api', mainRouterDefs.paths),
    },
}

// general express setup
const server = express();
server.use(helmet());
server.use(cors());

// parse application/x-www-form-urlencoded 
server.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

// parse application/json 
server.use(bodyParser.json({limit: '50mb'}));

// endpoint mapping
server.use('/api', mainRouter.handler());
server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openapiDoc));

export default server;
