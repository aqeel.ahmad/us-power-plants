import * as option from "fp-ts/lib/Option";
import { Umzug, SequelizeStorage } from 'umzug';
import { sequelize } from './orm'

export const migrator = new Umzug({
	migrations: {
		glob: [`${__dirname}/../migrations/*.*`, { cwd: __dirname }],
	},
	context: sequelize,
	storage: new SequelizeStorage({
		sequelize,
	}),
	logger: console,
});

export type Migration = typeof migrator._types.migration;

export const migrate = (async () => {
	// A table (and sequelize model) called SequelizeMeta is automatically 
	// created to track the applied migrations
    await migrator.up();
});

export type LineMapper<T> = (value: string[], line: number) => option.Option<T>

export type CSVReader<T> = (content: string, lm: LineMapper<T>) => Promise<option.Option<T>[]>
