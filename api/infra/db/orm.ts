import { Sequelize } from 'sequelize';

export const sequelize = new Sequelize({
	dialect: 'sqlite',
	storage: './db.sqlite',
	retry: {
		max: 20,
	}
});
