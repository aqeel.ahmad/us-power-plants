import * as option from "fp-ts/lib/Option";

import logger from "../utilities/logger";
import { PowerPlantRepository } from '../../domain/power-plant'
import { PowerPlantModel, mapperModelToPP } from './power-plant-model' 

const withPPSqlRepoImpl: PowerPlantRepository = {
    search: async (limit) => {
        return PowerPlantModel
            .findAll({ limit })
            .then((dbDataArray) =>
                dbDataArray
                    .map(model => mapperModelToPP(model)))
    },
    searchTop: async (n) => {
        return PowerPlantModel
            .findAll({ limit: n, order: [['annual_net_generation', 'DESC']]})
            .then((dbDataArray) =>
                dbDataArray
                    .map(model => mapperModelToPP(model)))
    },
    searchByState: async (state) => {
        return PowerPlantModel
            .findAll({ 
                where: { state }, 
                order: [['annual_net_generation', 'DESC']] })
            .then((dbDataArray) =>
                dbDataArray
                    .map(model => mapperModelToPP(model)))
    },
    get: async (id) => {
        return PowerPlantModel
            .findByPk(id)
            .then((model) => option.some(mapperModelToPP(model)))
            .catch(err => {
                logger.warn(`error requesting plant with id ${id}`, err)
                return option.none
            })
    },

    getUniqueStates: async () => {
        return PowerPlantModel
            .findAll({
                attributes: ['state'],
                group: ['state']
            })
            .then((dbDataArray) =>
                dbDataArray
                    .map(model => mapperModelToPP(model).state))
    },
};

export default withPPSqlRepoImpl;
