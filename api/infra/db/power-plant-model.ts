import { DataTypes, Model } from 'sequelize';
import { PowerPlant } from '../../domain/power-plant'
import { sequelize } from './orm'

// TODO: reduce the amt of duplication!
export class PowerPlantModel extends Model {
    declare id: string;
    declare name: string
    declare state: string
    declare annual_net_generation?: number
    declare latitude: number
    declare longitude: number
}

PowerPlantModel.init({
  id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
  },
  name: {
      type: DataTypes.STRING,
      allowNull: false
  },
  state: {
      type: DataTypes.STRING,
      allowNull: false
  },
  annual_net_generation: {
      type: DataTypes.NUMBER,
      allowNull: true
  },
  longitude: {
      type: DataTypes.NUMBER,
      allowNull: false
  },
  latitude: {
      type: DataTypes.NUMBER,
      allowNull: false
  },
  createdAt: {
      type: DataTypes.DATE,
      allowNull: false
  },
  updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
  }
}, { 
    tableName: 'power_plants', 
    sequelize,
});


export const mapperModelToPP =
    (model: PowerPlantModel) => <PowerPlant> {
        id: model.id,
        name: model.name,
        state: model.state,
        annual_net_generation: model.annual_net_generation,
        latitude: model.latitude,
        longitude: model.longitude,
}

export const mapperPPToModel =
    (pp: PowerPlant) => <PowerPlantModel> {
        id: pp.id,
        name: pp.name,
        state: pp.state,
        annual_net_generation: pp.annual_net_generation,
        longitude: pp.longitude,
        latitude: pp.latitude,
    }
